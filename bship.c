/****************************************************************************
* COSC2138/CPT220 - Programming Principles 2A
* Study Period 4  2014 Assignment #1 - battleship program
* Full Name        : Paolo Felicelli   
* Student Number   : s3427174
* Course Code      : CPT220
* Start up code provided by the CTeach Team
****************************************************************************/

#include "bship.h"

/****************************************************************************
* Function main() is the entry point for the program.
****************************************************************************/
int main(void)
{
   /* Stores player ship position information secret to opponent. */
   char playerHidden[SIZE][SIZE];
   /* Stores player ship position information known to opponent. */
   char playerReveal[SIZE][SIZE];
   /* Stores computer ship position information secret to opponent. */
   char computHidden[SIZE][SIZE];
   /* Stores computer ship position information known to opponent. */
   char computReveal[SIZE][SIZE];

   /* Initialize grids with UNKNOWN ' '. */
   init(playerHidden, playerReveal, computHidden, computReveal);

   /* Position player/computer ships. */
   placePlayerShips(playerHidden);
   placeComputerShips(computHidden);
   
   /* Game loops until user or computer wins. */
   do
   {
      displayKnownInfo(playerReveal, playerHidden, computReveal); 
      playerGuess(computHidden, computReveal);
      computerGuess(playerHidden, playerReveal);
   }
   while(isGameOver(playerHidden, playerReveal, 
         computHidden, computReveal) == FALSE);
   /* If user or computer wins, display all game info. */
   displayAllInfo(playerHidden, playerReveal, computHidden, computReveal);

   return EXIT_SUCCESS;
}


/****************************************************************************
* Function init() initialises every cell in the four grids to a safe default
* value. The UNKNOWN constant is used for the initialisation value.
****************************************************************************/
void init(char playerHidden[SIZE][SIZE], char playerReveal[SIZE][SIZE],
          char computHidden[SIZE][SIZE], char computReveal[SIZE][SIZE])
{
   /* Nested for loops initialize grids with UNKNOWN. */
   int i, j;
   for(i = 0; i < SIZE; i++)  
   {
      for(j = 0; j < SIZE; j++)
      {
         playerHidden[i][j] = UNKNOWN;
      }
   }

   for(i = 0; i < SIZE; i++)
   {
      for(j = 0; j < SIZE; j++)
      {
         playerReveal[i][j] = UNKNOWN;
      }
   }

   for(i = 0; i < SIZE; i++)
   {
      for(j = 0; j < SIZE; j++)
      {
         computHidden[i][j] = UNKNOWN;
      }
   }

   for(i = 0; i < SIZE; i++)
   {
      for(j = 0; j < SIZE; j++)
      {
         computReveal[i][j] = UNKNOWN;
      }
   }
   printf("Cells initialized.....\n\n");
}


/****************************************************************************
* Function placePlayerShips() adds the player ships to the grid in the
* following fixed pattern:
*    1 2 3 4 5 6 7 8 9 0
*   +-+-+-+-+-+-+-+-+-+-+
* a |A| | | | | | | | | |
*   +-+-+-+-+-+-+-+-+-+-+
* b |A| | | | | |D|D|D| |
*   +-+-+-+-+-+-+-+-+-+-+
* c |A| | | | | | |S| | |
*   +-+-+-+-+-+-+-+-+-+-+
* d |A| | | | | | |S| | |
*   +-+-+-+-+-+-+-+-+-+-+
* e |A| | | | | | | | | |
*   +-+-+-+-+-+-+-+-+-+-+
* f | | | | | | | | | | |
*   +-+-+-+-+-+-+-+-+-+-+
* g | | | | | | | | | | |
*   +-+-+-+-+-+-+-+-+-+-+
* h | | | | | | | | | | |
*   +-+-+-+-+-+-+-+-+-+-+
* i | |F|F|F| | | | | | |
*   +-+-+-+-+-+-+-+-+-+-+
* j | | | | | | |B|B|B|B|
*   +-+-+-+-+-+-+-+-+-+-+
****************************************************************************/
void placePlayerShips(char playerHidden[SIZE][SIZE])
{
   /* Add all player ships into position. */
   int i, j;
   for(i = 0; i < AIRCRAFT_CARRIER_LEN; i++)
   {
      playerHidden[i][0] = AIRCRAFT_CARRIER;
   }

   for(j = 6; j < BATTLESHIP_LEN + 6; j++)
   {
      playerHidden[9][j] = BATTLESHIP;
   }

   for(j = 6; j < DESTROYER_LEN + 6; j++)
   {
      playerHidden[1][j] = DESTROYER;
   }

   for(j = 1; j <= FRIGATE_LEN; j++)
   {
      playerHidden[8][j] = FRIGATE;
   }

   for(i = 2; i < SUBMARINE_LEN + 2; i++)
   {
      playerHidden[i][7] = SUBMARINE;
   } 
   printf("Player ships added to grid.....\n\n");
}


/****************************************************************************
* Function placeComputerShips() adds the computer ships to the grid in a
* similar pattern as the placePlayerShips() function except that the grid has
* been mirrored horizontally:
*    1 2 3 4 5 6 7 8 9 0
*   +-+-+-+-+-+-+-+-+-+-+
* a | | | | | | | | | |A|
*   +-+-+-+-+-+-+-+-+-+-+
* b | |D|D|D| | | | | |A|
*   +-+-+-+-+-+-+-+-+-+-+
* c | | |S| | | | | | |A|
*   +-+-+-+-+-+-+-+-+-+-+
* d | | |S| | | | | | |A|
*   +-+-+-+-+-+-+-+-+-+-+
* e | | | | | | | | | |A|
*   +-+-+-+-+-+-+-+-+-+-+
* f | | | | | | | | | | |
*   +-+-+-+-+-+-+-+-+-+-+
* g | | | | | | | | | | |
*   +-+-+-+-+-+-+-+-+-+-+
* h | | | | | | | | | | |
*   +-+-+-+-+-+-+-+-+-+-+
* i | | | | | | |F|F|F| |
*   +-+-+-+-+-+-+-+-+-+-+
* j |B|B|B|B| | | | | | |
*   +-+-+-+-+-+-+-+-+-+-+
****************************************************************************/
void placeComputerShips(char computHidden[SIZE][SIZE])
{
   /* Add all computer ships into position. */
   int i, j;
   for(i = 0; i < AIRCRAFT_CARRIER_LEN; i++)
   {
      computHidden[i][9] = AIRCRAFT_CARRIER;
   }

   for(j = 0; j < BATTLESHIP_LEN; j++)
   {
      computHidden[9][j] = BATTLESHIP;
   }

   for(j = 1; j <= DESTROYER_LEN; j++)
   {
      computHidden[1][j] = DESTROYER;
   }

   for(j = 6; j < FRIGATE_LEN + 6; j++)
   {
      computHidden[8][j] = FRIGATE;
   }

   for(i = 2; i < SUBMARINE_LEN + 2; i++)
   {
      computHidden[i][2] = SUBMARINE;
   }
   printf("Computer ships added to grid.....\n\n");
}


/****************************************************************************
* Function displayKnownInfo() presents revealed information about the game in
* the format below. In this example, both contestants have made five
* guesses. 
* As you can see, the computer player got lucky with all five guesses and has
* sunk the human players' aircraft carrier. The identity of the ship was
* revealed when the aircraft carrier was HIT the fifth time.
* The human player has been less lucky. The first four guesses were a MISS.
* However, the fifth guess was a HIT on the computer players' submarine. The
* human player does not yet know the identity of this ship yet as it is still
* afloat.
* All other squares are still UNKNOWN.
*
*          Player         |         Computer
*    1 2 3 4 5 6 7 8 9 0  |    1 2 3 4 5 6 7 8 9 0
*   +-+-+-+-+-+-+-+-+-+-+ |   +-+-+-+-+-+-+-+-+-+-+
* a |A| | | | | | | | | | | a | | | | | | | | | | |
*   +-+-+-+-+-+-+-+-+-+-+ |   +-+-+-+-+-+-+-+-+-+-+
* b |A| | | | | | | | | | | b | | | | | | | | | | |
*   +-+-+-+-+-+-+-+-+-+-+ |   +-+-+-+-+-+-+-+-+-+-+
* c |A| | | | | | | | | | | c | | | | | | |=| | | |
*   +-+-+-+-+-+-+-+-+-+-+ |   +-+-+-+-+-+-+-+-+-+-+
* d |A| | | | | | | | | | | d | | |x| | | | | | | |
*   +-+-+-+-+-+-+-+-+-+-+ |   +-+-+-+-+-+-+-+-+-+-+
* e |A| | | | | | | | | | | e | | | | | | | | | | |
*   +-+-+-+-+-+-+-+-+-+-+ |   +-+-+-+-+-+-+-+-+-+-+
* f | | | | | | | | | | | | f | | | | |=| | | | | |
*   +-+-+-+-+-+-+-+-+-+-+ |   +-+-+-+-+-+-+-+-+-+-+
* g | | | | | | | | | | | | g | | | | | | | | | | |
*   +-+-+-+-+-+-+-+-+-+-+ |   +-+-+-+-+-+-+-+-+-+-+
* h | | | | | | | | | | | | h | |=| | | | |=| | | |
*   +-+-+-+-+-+-+-+-+-+-+ |   +-+-+-+-+-+-+-+-+-+-+
* i | | | | | | | | | | | | i | | | | | | | | | | |
*   +-+-+-+-+-+-+-+-+-+-+ |   +-+-+-+-+-+-+-+-+-+-+
* j | | | | | | | | | | | | j | | | | | | | | | | |
*   +-+-+-+-+-+-+-+-+-+-+ |   +-+-+-+-+-+-+-+-+-+-+
* Aircraft Carrier  (5/5) | 0/5 ships sunk.
* Battleship        (0/4) | 1 hits.
* Destroyer         (0/3) | 4 misses.
* Frigate           (0/3) |
* Submarine         (0/2) |
****************************************************************************/
void displayKnownInfo(char playerReveal[SIZE][SIZE], 
                      char playerHidden[SIZE][SIZE],
                      char computReveal[SIZE][SIZE])
{
   /* Struct declared static to preserve the values stored between function calls. */
   static struct shipHitStats computerStats;
   /* Declare/initialize variables and struct. */
   char co_ordValue;
   static int numShipsSunk; /* numShipsSunk declared static to preserve the 
                                 increments between function calls. */
   int i, hit = 0, miss = 0; 
   struct shipHitStats playerStats = {0};

   char *playerCo_ord = &playerReveal[0][0];
   char *playerHiddenCo_ord = &playerHidden[0][0];
   char *computerCo_ord = &computReveal[0][0];

   /* Function call to dump known/updated info to screen. */
   dumpGrids(&playerReveal[0][0], &computReveal[0][0]);

   for(i = 0; i < CO_ORDS; i++)
   {
      /* Increment number of overall hits and misses. */
      if(*(computerCo_ord + i) == HIT)
      {
         hit++;
      }

      if(*(computerCo_ord + i) == MISS)
      {
         miss++;
      }

      /* Increment number of hits and misses for each ship in computerStats struct 
       * and increment amount of sunken ships. */
      if(*(computerCo_ord + i) == AIRCRAFT_CARRIER)
      {
         hit++;
         computerStats.carrHits++;
         if(computerStats.carrHits == AIRCRAFT_CARRIER_LEN)
         {
            numShipsSunk++;
         }
      }

      if(*(computerCo_ord + i) == BATTLESHIP)
      {
         hit++;
         computerStats.battHits++;
         if(computerStats.battHits == BATTLESHIP_LEN)
         {
            numShipsSunk++;
         }
      }

      if(*(computerCo_ord + i) == DESTROYER)
      {
         hit++;
         computerStats.destHits++;
         if(computerStats.destHits == DESTROYER_LEN)
         {
            numShipsSunk++;
         }
      }

      if(*(computerCo_ord + i) == FRIGATE)
      {
         hit++;
         computerStats.frigHits++;
         if(computerStats.frigHits == FRIGATE_LEN)
         {
            numShipsSunk++;
         }
      }

      if(*(computerCo_ord + i) == SUBMARINE)
      {
         hit++;
         computerStats.subHits++;
         if(computerStats.subHits == SUBMARINE_LEN)
         {
            numShipsSunk++;
         }
      }

      if(*(playerCo_ord + i) == MISS || *(playerCo_ord + i) == UNKNOWN)
      {
         continue;
      }
      else
      {
         co_ordValue = *(playerHiddenCo_ord + i);
      }

      /* Increment ship hits in the playerstats struct. */
      if(co_ordValue == AIRCRAFT_CARRIER)
      {
         playerStats.carrHits++;
      }

      if(co_ordValue == BATTLESHIP)
      {
         playerStats.battHits++;
      }

      if(co_ordValue == DESTROYER)
      {
         playerStats.destHits++;
      }

      if(co_ordValue == FRIGATE)
      {
         playerStats.frigHits++;
      }

      if(co_ordValue == SUBMARINE)
      {
         playerStats.subHits++;
      }
   }

   /* Dump formatted updated/known stats to the screen */
   printf(" Aircraft Carrier (%d/%d)  |   %d/%d ships sunk.\n", 
   playerStats.carrHits, AIRCRAFT_CARRIER_LEN, numShipsSunk, 5);

   printf(" Battleship       (%d/%d)  |   %d hits.\n",
   playerStats.battHits, BATTLESHIP_LEN, hit);

   printf(" Destroyer        (%d/%d)  |   %d misses\n",
   playerStats.destHits, DESTROYER_LEN, miss);

   printf(" Frigate          (%d/%d)\n", playerStats.frigHits, FRIGATE_LEN);
   
   printf(" Submarine        (%d/%d)\n\n", playerStats.subHits, SUBMARINE_LEN);
}


/****************************************************************************
* Function playerGuess() allows the user to guess the position of an
* opponents' ship by providing the cell reference. This guess information is
* recorded in the computReveal[][] variable. The user is given feedback about
* the accuracy of the guess. Additional feedback is given if the guess sinks
* the enemy ship. Example:
*
* Enter a grid reference (a1-j0): c3
* You hit an enemy ship.
* You sunk the enemy submarine.
****************************************************************************/
void playerGuess(char computHidden[SIZE][SIZE], 
                 char computReveal[SIZE][SIZE])
{
   /* Declare/initialize variables and struct */
   int row, column, isValid = 0;
   /* Struct declared static to preserve the values between function calls. */
   static struct shipHitStats stats;
   char cellRef[3];

   while(isValid == FALSE)
   {
      /* Function call to validate coordinate input. */
      validateInput(&cellRef[0]);

      /* tolower returns the ASCII value and sets it to row. */
      row = tolower(cellRef[0]);
      /* ASCII value of row subtracts the ASCII value of 'a' and gives the
       * int value needed for computReveal array. */
      row = row - LOWA;

      /* If user inputs column 0 as a coordinate, computReveal will refer to
       * element 9 in the array. */
      if(cellRef[1] == ZERO)
      {
         column = SIZE - 1;
      }
      /* else refer to the preceding elements in the array. */
      else
      {
         column = cellRef[1] - ZERO - 1;
      }

      /* If user has already targeted this coordinate, then print message. */
      if(computReveal[row][column] != UNKNOWN)
      {
         printf("You have already targeted this coordinate......\n");
         continue;
      }
      isValid = TRUE;

      /* If user has hit a ship, print message. */
      if(computHidden[row][column] != UNKNOWN)
      {
         computReveal[row][column] = HIT;
         printf("You hit an enemy ship!\n");

         /* Increment the amount of hits for corresponding ship. If amount of hits is
          * equal to ship length, print message and call showHits function. */
         if(computHidden[row][column] == AIRCRAFT_CARRIER)
         {
            stats.carrHits++;
            if(stats.carrHits == AIRCRAFT_CARRIER_LEN)
            {
               printf("You sunk the enemy Aircraft carrier!\n");
               showShips(computHidden, computReveal, AIRCRAFT_CARRIER);
            }
            break;
         }

         if(computHidden[row][column] == BATTLESHIP)
         {
            stats.battHits++;
            if(stats.battHits == BATTLESHIP_LEN)
            {
               printf("You sunk the enemy Battleship!\n");
               showShips(computHidden, computReveal, BATTLESHIP);
            }
            break;
         }

         if(computHidden[row][column] == DESTROYER)
         {
            stats.destHits++;
            if(stats.destHits == DESTROYER_LEN)
            {
               printf("You sunk the enemy Destroyer!\n");
               showShips(computHidden, computReveal, DESTROYER);
            }
            break;
         }

         if(computHidden[row][column] == FRIGATE)
         {
            stats.frigHits++;
            if(stats.frigHits == FRIGATE_LEN)
            {
               printf("You sunk the enemy Frigate!\n");
               showShips(computHidden, computReveal, FRIGATE);
            }
            break;
         }

         if(computHidden[row][column] == SUBMARINE)
         {
            stats.subHits++;
            if(stats.subHits == SUBMARINE_LEN)
            {
               printf("You sunk the enemy Submarine!\n");
               showShips(computHidden, computReveal, SUBMARINE);
            }
            break;
         }
      }

      else
      {
         computReveal[row][column] = MISS;
         printf("You missed.....\n");
      }
      printf("\n");
   }
}


/****************************************************************************
* Function computerGuess() has a lot in common with the playerGuess()
* function. This function allows the computer-controlled opponent to randomly
* guess the positions of the players' ships. The guess information is stored
* in the playerReveal[][] variable. Any square that hasn't been previously
* guessed is selected. The user is given feedback about the accuracy of the
* guess. Additional feedback is given if the guess sinks one of your ships.
* Example:
*
* Opponent guessed square 'f9'. use srand()
* Opponent missed your ships.
*
* This function uses the s_rand() and rand() standard library functions to
* implement random number generation.
****************************************************************************/
void computerGuess(char playerHidden[SIZE][SIZE], 
                   char playerReveal[SIZE][SIZE])
{
   /* Declare/initialize variables and struct */
   int row, column, isValid = 0;
   /* Struct declared static to preserve the values between function calls. */
   static struct shipHitStats playerStats;
   
   while(isValid == FALSE)
   {
      /* rand() returns random numbers. */
      row = rand() % 10;
      column = rand() % 10;
      
      if(playerReveal[row][column] == UNKNOWN)
      {
         isValid = TRUE;
      }

      printf("Opponent guessed square %c%d\n", LOWA + row, column);

      /* If computer hits a ship, print message. */
      if(playerHidden[row][column] != UNKNOWN)
      {
         printf("Computer has hit your ships.....\n\n");
         playerReveal[row][column] = HIT;
      }

      /* Increment the amount of hits for corresponding ship. If amount of hits is
       * equal to ship length, print message and call showHits function. */
      if(playerHidden[row][column] == AIRCRAFT_CARRIER)
      {
         playerStats.carrHits++;
         if(playerStats.carrHits == AIRCRAFT_CARRIER_LEN)
         {
            printf("Computer has sunk your Aircraft Carrier!\n");
            showShips(playerHidden, playerReveal, AIRCRAFT_CARRIER);
         }
         break;
      }

      if(playerHidden[row][column] == BATTLESHIP)
      {
         playerStats.battHits++;
         if(playerStats.battHits == BATTLESHIP_LEN)
         {
            printf("Computer has sunk your Battleship!\n");
            showShips(playerHidden, playerReveal, BATTLESHIP);
         }
         break;
      }

      if(playerHidden[row][column] == DESTROYER)
      {
         playerStats.destHits++;
         if(playerStats.destHits == DESTROYER_LEN)
         {
            printf("Computer has sunk your Destroyer!\n");
            showShips(playerHidden, playerReveal, DESTROYER);
         }
         break;
      }

      if(playerHidden[row][column] == FRIGATE)
      {
         playerStats.frigHits++;
         if(playerStats.frigHits == FRIGATE_LEN)
         {
            printf("Computer has sunk your Frigate!\n");
            showShips(playerHidden, playerReveal, FRIGATE);
         }
         break;
      }

      if(playerHidden[row][column] == SUBMARINE)
      {
         playerStats.subHits++;
         if(playerStats.subHits == SUBMARINE_LEN)
         {
            printf("Computer has sunk your Submarine!\n");
            showShips(playerHidden, playerReveal, SUBMARINE);
         }
         break;
      }

      else
      {
         printf("Opponent missed your ships.....\n\n");
         playerReveal[row][column] = MISS;
      }
   }
}


/****************************************************************************
* Function isGameOver() checks to see if a threshold number of ships has been
* sunk by either the user or computer-controlled opponent (use SHIPS_TO_SINK
* constant). The function returns TRUE or FALSE.
****************************************************************************/
int isGameOver(char playerHidden[SIZE][SIZE], char playerReveal[SIZE][SIZE],
               char computHidden[SIZE][SIZE], char computReveal[SIZE][SIZE])
{
   /* Function call to get how many ships are sunken. */
   int playerShips = getShips(playerReveal);
   int computerShips = getShips(computReveal);

   /* If amount of user or computer ships is equal to 3, then game over... */
   if(playerShips == SHIPS_TO_SINK || computerShips == SHIPS_TO_SINK)
   {
      /* printf("game over\n"); */  
      return TRUE;
   }
   else
   {
      /* printf("game not over\n"); */ 
      return FALSE;
   }
}


/****************************************************************************
* Function displayAllInfo() is called at the end of the game and provides
* information about the outcome. The output is very similar to that of the
* displayKnownInfo() function. This function additionally reveals all 
* ship locations with unrevealed ship positions represented in lower case
* letters. Finally, a congratulatory or commiserative message is displayed to
* the user depending on the outcome of the game. Example:
*
*          Player         |         Computer
*    1 2 3 4 5 6 7 8 9 0  |    1 2 3 4 5 6 7 8 9 0
*   +-+-+-+-+-+-+-+-+-+-+ |   +-+-+-+-+-+-+-+-+-+-+
* a |A| | | | | | | | | | | a | | | | | | | | | |a|
*   +-+-+-+-+-+-+-+-+-+-+ |   +-+-+-+-+-+-+-+-+-+-+
* b |A| | | |=| |d|d|d| | | b | |d|d|d| | | | | |a|
*   +-+-+-+-+-+-+-+-+-+-+ |   +-+-+-+-+-+-+-+-+-+-+
* c |A| | | | | | |s| | | | c | | |S| | | |=| | |a|
*   +-+-+-+-+-+-+-+-+-+-+ |   +-+-+-+-+-+-+-+-+-+-+
* d |A| | | | | | |s| | | | d | | |S| | | | | | |a|
*   +-+-+-+-+-+-+-+-+-+-+ |   +-+-+-+-+-+-+-+-+-+-+
* e |A| | | | | | | | | | | e | | | | | | | | | |a|
*   +-+-+-+-+-+-+-+-+-+-+ |   +-+-+-+-+-+-+-+-+-+-+
* f | | | | | | | | |=| | | f | | | | |=| | | | | |
*   +-+-+-+-+-+-+-+-+-+-+ |   +-+-+-+-+-+-+-+-+-+-+
* g | | | | | | | | | | | | g | | | | | | | | | | |
*   +-+-+-+-+-+-+-+-+-+-+ |   +-+-+-+-+-+-+-+-+-+-+
* h | |=| | | | | | |=| | | h | |=| | | | |=| | | |
*   +-+-+-+-+-+-+-+-+-+-+ |   +-+-+-+-+-+-+-+-+-+-+
* i |=|F|F|f| | | | | | | | i | | | | | | |F|F|F| |
*   +-+-+-+-+-+-+-+-+-+-+ |   +-+-+-+-+-+-+-+-+-+-+
* j | |=| | | | |b|b|b|b| | j |B|B|B|B| | | | | | |
*   +-+-+-+-+-+-+-+-+-+-+ |   +-+-+-+-+-+-+-+-+-+-+
* Aircraft Carrier  (5/5) | 3/5 ships sunk.
* Battleship        (0/4) | 9 hits.
* Destroyer         (0/3) | 4 misses.
* Frigate           (2/3) |
* Submarine         (0/2) |
*           YOU WON - CONGRATULATIONS !!!
****************************************************************************/
void displayAllInfo(char playerHidden[SIZE][SIZE],
                    char playerReveal[SIZE][SIZE],
                    char computHidden[SIZE][SIZE],
                    char computReveal[SIZE][SIZE])
{
   /* Function call to getShips, get how many ships are sunken. */
   int playerShips = getShips(playerReveal);
   int computerShips = getShips(computReveal);

   /* Show formatted ship stats and reveal grids. */
   displayKnownInfo(playerReveal, playerHidden, computReveal);

   if(computerShips > playerShips)
   {
      printf("  YOU WON - CONGRATULATIONS !!!\n");
   }
   else
   {
      printf("  YOU LOSE.....\n");
   }
}


/****************************************************************************
* getString(): An interactive string input function with dynamic memory.
* This function prompts the user for a string using a custom prompt. A line
* of text is accepted from the user using fgets() and stored in a temporary
* string. When the function detects that the user has entered too much text,
* an error message is given and the user is forced to reenter the input. The
* function also clears the extra text (if any) with the readRestOfLine()
* function.
* When a valid string has been accepted, the unnecessary newline character
* at the end of the string is overwritten. Finally, the temporary string is
* copied to the string variable that is returned to the calling function.
****************************************************************************
* Note: Portions modified from https://www.dlsweb.rmit.edu.au/set/Courses/
* Content/CSIT/oua/cpt220/c-function-examples/InputValidation/
* getString-advanced.c
****************************************************************************/
int getString(char *string, int length, int minlength, char *prompt)
{
   int finished = FALSE;
   char tempString[3];

   /* Allocate temporary memory. */
   /*if ((tempString = malloc(sizeof(char) * (length+2))) == NULL)
   {
      fprintf(stderr, "Fatal error: malloc() failed in getString().\n");
      exit(EXIT_FAILURE);
   }*/

   /* Continue to interact with the user until the input is valid. */
   do
   {
      /* Provide a custom prompt. */
      printf("%s", prompt);

      /* Accept input. "+2" is for the \n and \0 characters. */
      fgets(tempString, length + 2, stdin);

      /* A string that doesn't have a newline character is too long. */
      if (tempString[strlen(tempString) - 1] != '\n' )
      {
         printf("Input was too long.....\n");
         readRestOfLine();
      }
      else if(strlen(tempString) < minlength + 1)
      {
         printf("Input was too short.....\n");
      }
      else
      {
         finished = TRUE;
      }

   }
   while (finished == FALSE);

   /* Overwrite the \n character with \0. */
   tempString[strlen(tempString) - 1] = '\0';

   /* Make the result string available to calling function. */
   strcpy(string, tempString);

   /* Deallocate temporary memory. */
   /* free(tempString); */

   return SUCCESS;
}


/****************************************************************************
* Function readRestOfLine() is used for buffer clearing.
* FrequentlyAskedQuestions/#alternative
****************************************************************************/
void readRestOfLine()
{
   int c;

   /* Read until the end of the line or end-of-file. */   
   while ((c = fgetc(stdin)) != '\n' && c != EOF)
      ;

   /* Clear the error and end-of-file flags. */
   clearerr(stdin);
}


/*****************************************************************************
* validateInput function checks whether the user input is a valid character 
* between a - j and 1 - 0.
*****************************************************************************/
void validateInput(char *cellRef)
{
   int isValid = 0;

   while(isValid == FALSE)
   {
      getString(cellRef, 2, 2, "Enter a grid reference (a1-j0): ");

      /* If input is not between a and j, print message and try again. */ 
      if(cellRef[0] < LOWA || cellRef[0] > LOWJ)
      {
         printf("Invalid row input, must be lowercase (a - j).....\n");
         continue;
      }
      /* If input is not between 1 and 0, print message and try again. */ 
      if(cellRef[1] < ZERO || cellRef[1] > NINE)
      {
         printf("Invalid column input, must be a digit (1 - 0).....\n");
         continue;
      }
      isValid = TRUE;
   }
}


/***************************************************************************
* dumpGrids() dumps the player/computer grids with the corresponding cell
* values when displayKnownInfo is called.
***************************************************************************/
void dumpGrids(char *playerGrid, char *computGrid)
{   
   int i, j, numOfGrids = 2, row = 0; 
   char c = 'a';

   /* Print out top numbers and heading for player/computer grids. */
   printf("          Player         |         Computer       \n");
   printf("    ");
   for(i = 1; i <= SIZE; i++)
   {
      /* Print the numbers 1 - 9 on top of left grid. */
      if(i != SIZE)
      {
         printf("%d ", i);
      }

      else if(i == SIZE)
      {
         /* Print the 0 on the end of the left and pipe separator. */
         printf("%d  |", i - SIZE);
         printf("    ");
         
         for(j = 1; j <= SIZE; j++)
         {
            /* Print the numbers 1 - 9 on top of right grid. */
            if(j != SIZE)
            {
               printf("%d ", j);
            }
            /* Print the 0 on the end of the right grid. */
            else if(j == SIZE)
            {
               printf("%d  \n", i - SIZE);
            }
         }
      }
   }

   /* Print a - j and +- characters for each numOfGrids iteration
    * for both grids. */
   while(row <= SIZE)
   {
      for(i = 0; i < numOfGrids; i++)
      {
         printf("   ");
         for(j = 0; j < SIZE; j++)
         {
            printf("+-");
         }
         printf("+");

         if(i != 1) 
         {
            printf(" |");
         }
      }
      printf("\n");

      for(i = 0; i < numOfGrids; i++)
      {
         if(row < SIZE)
         {
            printf(" %c ", c);

            for(j = 0; j < SIZE; j++)
            {
               /* Print the pipe separators for left grid. */
               if(i == 0)
               {
                  printf("|%c", *(playerGrid + (row * SIZE) + j));
               }
               /* Print the pipe separators for right grid. */
               else
               {
                  printf("|%c", *(computGrid + (row * SIZE) + j));
               }
            }
            printf("|");

            if(i != 1)
            {
               printf(" |");
            }
         }
      }
      printf("\n");
      row++;
      c++;
   }
}


/*********************************************************
* showShips() sets grid pointer to passed in type of ship
**********************************************************/
   
void showShips(char hidden[SIZE][SIZE], char shown[SIZE][SIZE], char typeOfShip)
{
   int i;
   char *hiddenPointer = &hidden[0][0];
   char *shownPointer = &shown[0][0];

   /* Loop through all coordinates, if pointer == typeOfShip set ship
    * to that location. */
   for(i = 0; i < CO_ORDS; i++)
   {
      if(*(hiddenPointer + i) == typeOfShip)
      {
         *(shownPointer + i) = typeOfShip;
      }
   }
}


/***************************************************************************
* getSunkShips() checks if hits == ship length and how many ships have been 
* sunk.
***************************************************************************/
int getShips(char grid[SIZE][SIZE])
{
   /* Declare/initialize variables and struct */
   int i, sunkShips = 0;
   struct shipHitStats stats = {0};
   char *ptr = &grid[0][0];
   
   /* Loop through every coordinate, add up hits and return the checked 
    * amount of sunken ships. */
   for(i = 0; i < CO_ORDS; i++)
   {
      if(*(ptr + i) == AIRCRAFT_CARRIER)
      {
         stats.carrHits++;
         if(stats.carrHits == AIRCRAFT_CARRIER_LEN)
         {
            sunkShips++;
         }
      }

      if(*(ptr + i) == BATTLESHIP)
      {
         stats.battHits++;
         if(stats.battHits == BATTLESHIP_LEN)
         {
            sunkShips++;
         }
      }

      if(*(ptr + i) == DESTROYER)
      {
         stats.destHits++;
         if(stats.destHits == DESTROYER_LEN)
         {
            sunkShips++;
         }
      }

      if(*(ptr + i) == FRIGATE)
      {
         stats.frigHits++;
         if(stats.frigHits == FRIGATE_LEN)
         {
            sunkShips++;
         }
      }

      if(*(ptr + i) == SUBMARINE)
      {
         stats.subHits++;
         if(stats.subHits == SUBMARINE_LEN)
         {
            sunkShips++;
         }
      }
   }
   return sunkShips;
}





