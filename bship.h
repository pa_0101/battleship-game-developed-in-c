/****************************************************************************
* COSC2138/CPT220 - Programming Principles 2A
* Study Period 4  2014 Assignment #1 - battleship program
* Full Name        : Paolo Felicelli  
* Student Number   : s3427174
* Course Code      : CPT220
* Start up code provided by the CTeach team
****************************************************************************/

/* Header files. */
#include <stdio.h>
#include <stdlib.h>
/* My #includes. */
/* string.h has been included to provide the strlen() and strcpy() functions. */
#include <string.h>
/* ctype.h has been included to provide the tolower() function. */
#include <ctype.h>

/* Constants. */
#define SIZE 10
#define SHIPS_TO_SINK 3
#define HIT 'x'
#define MISS '='
#define UNKNOWN ' '
#define TRUE 1
#define FALSE 0

#define AIRCRAFT_CARRIER 'A'
#define BATTLESHIP 'B'
#define DESTROYER 'D'
#define FRIGATE 'F'
#define SUBMARINE 'S'

#define AIRCRAFT_CARRIER_LEN 5
#define BATTLESHIP_LEN 4
#define DESTROYER_LEN 3
#define FRIGATE_LEN 3
#define SUBMARINE_LEN 2

/* My constants. */
/* COORDS is the number of cells in each grid. */ 
#define CO_ORDS 100
/* SUCCESS is the return value for getString. */
#define SUCCESS 1
/* These constants are for checking 
 * column input in getValidInput and playerGuess functions. */
#define LOWA 'a'
#define LOWJ 'j'
#define ZERO '0'
#define NINE '9'

/* Function prototypes. */
void init(char playerHidden[SIZE][SIZE], char playerReveal[SIZE][SIZE],
          char computHidden[SIZE][SIZE], char computReveal[SIZE][SIZE]);
void placePlayerShips(char playerHidden[SIZE][SIZE]);
void placeComputerShips(char computHidden[SIZE][SIZE]);
void displayKnownInfo(char playerReveal[SIZE][SIZE], 
                      char playerHidden[SIZE][SIZE],
                      char computReveal[SIZE][SIZE]);
void playerGuess(char computHidden[SIZE][SIZE], 
                 char computReveal[SIZE][SIZE]);
void computerGuess(char playerHidden[SIZE][SIZE], 
                   char playerReveal[SIZE][SIZE]);
int isGameOver(char playerHidden[SIZE][SIZE], char playerReveal[SIZE][SIZE],
               char computHidden[SIZE][SIZE], char computReveal[SIZE][SIZE]);
void displayAllInfo(char playerHidden[SIZE][SIZE],
                    char playerReveal[SIZE][SIZE],
                    char computHidden[SIZE][SIZE],
                    char computReveal[SIZE][SIZE]);
void readRestOfLine();

/* My fuction prototypes. */
void dumpGrids(char *playerGrid, char *computGrid);
void validateInput(char *cellRef);
void showShips(char hidden[SIZE][SIZE], char shown[SIZE][SIZE], char shipType);
int getShips(char grid[SIZE][SIZE]);
/* Borrowed prototype */
int getString(char *string, int length, int minlength, char *prompt);

/* My structs. */
/* Member variables of the shipHitStats struct hold the number of hits that 
 * have been inflicted on each ship. */
struct shipHitStats{
   int carrHits, battHits, destHits, frigHits, subHits;
};








